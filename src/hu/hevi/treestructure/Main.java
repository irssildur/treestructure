package hu.hevi.treestructure;
import hu.hevi.treestructure.domain.Node;
import hu.hevi.treestructure.service.tree.TreeBuilderService;
import hu.hevi.treestructure.service.tree.TreeWalkerService;

import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        List<String> testStrings = new ArrayList<String>();
        testStrings.add("Category1|SubCategory1|SubSubCategory1");
        testStrings.add("Category1|SubCategory2|SubSubCategory1");
        testStrings.add("Category2|SubCategory1|SubSubCategory1");
        testStrings.add("Category2|SubCategory1|SubSubCategory1|SubSubSubCategory1");
        testStrings.add("Category2|SubCategory1|SubSubCategory1");
        testStrings.add("Category3|SubCategory1");
        testStrings.add("Category3|SubCategory1|SubSubCategory1");

        TreeBuilderService treeBuilder = new TreeBuilderService();
        Node root = new Node();
        for (String string : testStrings) {
            treeBuilder.addNodeToNextLevel(root, string);
        }

        System.out.println(root.toString());
        
        TreeWalkerService treeWalkerService = new TreeWalkerService();
        
        for (Node node : root.getChildNodes()) {
        	Integer numberOfChildNodes = treeWalkerService.countChildNodes(node);
        	StringBuilder stringBuilder = new StringBuilder();
        	stringBuilder.append(node.getId());
        	stringBuilder.append(" - ");
        	stringBuilder.append(node.getName());
        	stringBuilder.append(" - ");
        	stringBuilder.append(numberOfChildNodes);
        	System.out.println(stringBuilder.toString());
		}
        

    }
}
