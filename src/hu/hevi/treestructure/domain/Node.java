package hu.hevi.treestructure.domain;
import java.util.ArrayList;
import java.util.List;

public class Node {
    private int id;
    private String name;
    private List<Node> childNodes = new ArrayList<Node>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Node> getChildNodes() {
        return childNodes;
    }

    public void setChildNodes(List<Node> childNodes) {
        this.childNodes = childNodes;
    }
    
    public void addNode(Node node) {
    	childNodes.add(node);
    }

    @Override
    public String toString() {
        return "Node [id=" + id + ", name=" + name + ", childNodes=" + childNodes + "] \n";
    }

}
