package hu.hevi.treestructure.service.tree;
import hu.hevi.treestructure.domain.Node;
import hu.hevi.treestructure.service.id.IdGeneratorService;

import java.util.List;

public class TreeBuilderService {

    public void addNodeToNextLevel(Node root, String string) {
        String category = getCategoryForThisLevel(string);
        String rest = getCategoriesForNextLevel(string);

        if (isAnyCategoryLeft(category)) {
            Node nodeToAddTo = searchForCategoryWithName(root, category);
            if (nodeToAddTo == null) {
                nodeToAddTo = addNodeToSameLevel(root, category);
            }

            if (!rest.isEmpty()) {
                addNodeToNextLevel(nodeToAddTo, rest);
            }

        }
    }

    private Node searchForCategoryWithName(Node root, String name) {
        List<Node> nodes = root.getChildNodes();
        Node matchingNode = null;
        for (Node node : nodes) {
            if (node.getName().equals(name)) {
                matchingNode = node;
                break;
            }
        }
        return matchingNode;
    }

    private Node addNodeToSameLevel(Node root, String node) {
        Node newNode = new Node();
        newNode.setId(IdGeneratorService.getNextId());
        newNode.setName(node);
        root.getChildNodes().add(newNode);
        return newNode;
    }

    private String getCategoryForThisLevel(String string) {
        String categoryString = "";
        if (isStringContainsDelimiter(string)) {
            String[] nodes = string.split("\\|", 2);
            categoryString = nodes[0];
        } else if (!string.isEmpty()) {
            categoryString = string;
        }
        return categoryString;
    }

    private String getCategoriesForNextLevel(String string) {
        String restString = "";
        if (isStringContainsDelimiter(string)) {
            String[] nodes = string.split("\\|", 2);
            restString = nodes[1];
        }

        return restString;
    }

    private boolean isStringContainsDelimiter(String string) {
        return string.contains("|");
    }

    private boolean isAnyCategoryLeft(String category) {
        return category != "";
    }
}
