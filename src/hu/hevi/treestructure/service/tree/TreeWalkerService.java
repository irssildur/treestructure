package hu.hevi.treestructure.service.tree;

import hu.hevi.treestructure.domain.Node;

public class TreeWalkerService {
	public Integer countChildNodes(Node root) {
		Integer numberOfChildren = 0;
		numberOfChildren = root.getChildNodes().size();
		for (Node node : root.getChildNodes()) {
			numberOfChildren += countChildNodes(node);
		}
		return numberOfChildren;
	}
}
