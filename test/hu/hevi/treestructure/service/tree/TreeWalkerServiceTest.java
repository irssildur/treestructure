package hu.hevi.treestructure.service.tree;

import static org.junit.Assert.*;
import hu.hevi.treestructure.domain.Node;

import org.junit.Before;
import org.junit.Test;

public class TreeWalkerServiceTest {
	
	private TreeWalkerService underTest;

	@Before
	public void setUp() throws Exception {
		underTest = new TreeWalkerService();
	}

	@Test
	public void testCountChildNodesWithOneNodeShouldReturnOne() {
		// GIVEN
		Integer expected = 1;
		Node root = new Node();
		Node firstNode = new Node();
		root.addNode(firstNode);
		// WHEN
		Integer actual = underTest.countChildNodes(root);
		// THEN
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCountChildNodesWithTwoNodeShouldReturnTwo() {
		// GIVEN
		Integer expected = 2;
		Node root = new Node();
		Node firstNode = new Node();
		Node secondNode = new Node();
		root.addNode(firstNode);
		root.addNode(secondNode);
		// WHEN
		Integer actual = underTest.countChildNodes(root);
		// THEN
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCountChildNodesWithTwoNodeWithOneNodesShouldReturnFour() {
		// GIVEN
		Integer expected = 4;
		Node root = new Node();
		Node firstNode = new Node();
		Node secondNode = new Node();
		Node firstSubNode = new Node();
		Node secondSubNode = new Node();
		firstNode.addNode(firstSubNode);
		secondNode.addNode(secondSubNode);
		root.addNode(firstNode);
		root.addNode(secondNode);
		// WHEN
		Integer actual = underTest.countChildNodes(root);
		// THEN
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCountChildNodesWithTwoNodeWithOneNodesWithOneSubnodeShouldReturnFive() {
		// GIVEN
		Integer expected = 5;
		Node root = new Node();
		Node firstNode = new Node();
		Node secondNode = new Node();
		Node firstSubNode = new Node();
		Node firstSubSubNode = new Node();
		Node secondSubNode = new Node();
		firstSubNode.addNode(firstSubSubNode);
		firstNode.addNode(firstSubNode);
		secondNode.addNode(secondSubNode);
		root.addNode(firstNode);
		root.addNode(secondNode);
		// WHEN
		Integer actual = underTest.countChildNodes(root);
		// THEN
		assertEquals(expected, actual);
	}

}
