package hu.hevi.treestructure.service.tree;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;


import hu.hevi.treestructure.domain.Node;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class TreeBuilderServiceTest {

	private TreeBuilderService underTest;
	
	@Before
	public void setUp() throws Exception {
		underTest = new TreeBuilderService();
	}

	@Test
	public void testAddNodeToNextLevelWithOneStringShouldReturnOneToEachLevel() {
		// GIVEN
		Integer expectedSizeOnFirstLevel = 1;
		Integer expectedSizeOnSecondLevel = 1;
		Integer expectedSizeOnThirdLevel = 1;
		
		String testString = "Category1|SubCategory1|SubSubCategory1";
		Node root = new Node();
		
		// WHEN
		underTest.addNodeToNextLevel(root, testString);
		
		Node firstLevel = root;
		Node secondLevelNode = root.getChildNodes().get(0);
		Node thirdLevelNode = secondLevelNode.getChildNodes().get(0);
		
		Integer actualSizeOnFirstLevel = firstLevel.getChildNodes().size();
		Integer actualSizeOnSecondLevel = secondLevelNode.getChildNodes().size();
		Integer actualSizeOnThirdLevel = thirdLevelNode.getChildNodes().size();
		
		// THEN
		assertEquals(expectedSizeOnFirstLevel, actualSizeOnFirstLevel);
		assertEquals(expectedSizeOnSecondLevel, actualSizeOnSecondLevel);
		assertEquals(expectedSizeOnThirdLevel, actualSizeOnThirdLevel);
	}
	
	@Test
	public void testAddNodeToNextLevelWithOneStringShouldReturnOneOnFirstLevelAndTwoOnSecondAndThird() {
		// GIVEN
		Integer expectedSizeOnFirstLevel = 1;
		Integer expectedSizeOnSecondLevel = 2;
		Integer expectedSizeOnThirdLevel1 = 1;
		Integer expectedSizeOnThirdLevel2 = 1;
		List<String> testStrings = new ArrayList<String>();
		testStrings.add("Category1|SubCategory1|SubSubCategory1");
		testStrings.add("Category1|SubCategory2|SubSubCategory2");
		Node root = new Node();
		// WHEN
		for (String string : testStrings) {
			underTest.addNodeToNextLevel(root, string);
		}
		Node firstLevel = root.getChildNodes().get(0);
		Node secondLevelNode1 = firstLevel.getChildNodes().get(0);
		Node secondLevelNode2 = firstLevel.getChildNodes().get(1);
		
		Integer actualSizeOfFirstLevel = root.getChildNodes().size();
		Integer actualSizeOfSecondLevel = firstLevel.getChildNodes().size();
		Integer actualSizeOfThirdLevel1 = secondLevelNode1.getChildNodes().size();
		Integer actualSizeOfThirdLevel2 = secondLevelNode2.getChildNodes().size();
		// THEN
		assertEquals(expectedSizeOnFirstLevel, actualSizeOfFirstLevel);
		assertEquals(expectedSizeOnSecondLevel, actualSizeOfSecondLevel);
		assertEquals(expectedSizeOnThirdLevel1, actualSizeOfThirdLevel1);
		assertEquals(expectedSizeOnThirdLevel2, actualSizeOfThirdLevel2);
	}

}
